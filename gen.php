<?php

const IMAGE_FORMAT = 'jpeg';

/** Размер шрифта на картинке капчи */
const BASE_FONT_SIZE = 12;

/** Минимальное значение цвета для фона */
const BACKGROUND_COLOR_MIN = 0xeeeeee;

/** Максимальное значение цвета для фона */
const BACKGROUND_COLOR_MAX = 0x0;

/** Начальная координта по оси X */
const START_X_COORDINATE = 10;


/**
 * Генерирует и возвращает хеш цвета фона капчи
 *
 * @return string Хеш цвета фона капчи
 */
function generateBackgroundColor()
{
    return sprintf('#%06X', mt_rand(BACKGROUND_COLOR_MIN, BACKGROUND_COLOR_MAX));
}

/**
 * Отрисовывает на картинке линию
 *
 * @param \ImagickDraw $drawer Рисовальщик
 * @param \Imagick $image Картинка
 * @param int $strokeWidth Ширина отрисовываемой линии
 * @param bool $isStrict Если true - отрисовывает прямую, иначе - кривую
 * @param int $border Размер отступа от края картинки
 */
function drawLine(\ImagickDraw $drawer, \Imagick $image, $strokeWidth, $isStrict = true, $border = 10)
{
    $drawer->setStrokeWidth($strokeWidth);
    if ($isStrict) {
        $drawer->line(
            mt_rand($border, $image->getImageWidth()),
            mt_rand($border, $image->getImageHeight()),
            mt_rand($border, $image->getImageWidth()),
            mt_rand($border, $image->getImageHeight())
        );

        return;
    }

    $drawer->bezier([
        ['x' => mt_rand($border, $image->getImageWidth()), 'y' => mt_rand($border, $image->getImageHeight())],
        ['x' => mt_rand($border, $image->getImageWidth()), 'y' => mt_rand($border, $image->getImageHeight())],
        ['x' => mt_rand($border, $image->getImageWidth()), 'y' => mt_rand($border, $image->getImageHeight())],
    ]);
}

/**
 * Добавляет слой с шумом на картинку
 *
 * @param \Imagick $image Картинка
 *
 * @return \Imagick Картинка с наложенным слоем шума
 */
function addNoiseLayer(\Imagick $image)
{
    $noise = new \Imagick();
    $noise->newImage($image->getImageWidth(), $image->getImageHeight(), 'transparent');

    return $image->mergeImageLayers(\Imagick::LAYERMETHOD_COMPAREOVERLAY);
}

/**
 * Создает и возвращает картинку
 *
 * @param String $phrase Фраза
 *
 * @return \Imagick Объект изображения Imagick
 */
function createImage($phrase)
{
    $image = new \Imagick();
    $width = 200;
    $height = 300;

    $smallWidth = 100;
    $smallHeight = 150;

    $image->newImage($width, $height, new \ImagickPixel(generateBackgroundColor()));

    $imageDraw = new \ImagickDraw();
    $imageDraw->setFont($_SERVER['DOCUMENT_ROOT'] . '/fonts/fast-font.ttf');

    $heightCenter = $height / 2;
    $coordinateX = START_X_COORDINATE;

    for ($i = 0, $max = strlen($phrase); $i < $max; ++$i) {
        $textHeight = $image->queryFontMetrics($imageDraw, $phrase{$i})['textHeight'];
        $coordinateY = $heightCenter + ($textHeight * 0.4);
        $image->annotateImage($imageDraw, $coordinateX, $coordinateY, mt_rand(-5, 5), $phrase{$i});
        $coordinateX += $textHeight + 2;
    }

    $arr = [mt_rand(-80, 0), mt_rand(0, 80)];
    $roll = mt_rand(0, 25);

    $image->rollImage($roll, 0);
//        $image->swirlImage(mt_rand(90, 100) * array_rand([-1, 1]));
    $image->rollImage(-$roll * 3, 0);
    $image->swirlImage($arr[mt_rand(0, 1)]);
    
    $image->trimImage(0);
    $image->thumbnailImage($smallWidth, $smallHeight);
    $image->setImageFormat(IMAGE_FORMAT);
    $image->getImageBlob();
    $image = addNoiseLayer($image);

    $image->drawImage($imageDraw);
    header('Content-Type: image/' . IMAGE_FORMAT);
    echo $image->getImageBlob();

    return $image;
}


createImage('TEST');
